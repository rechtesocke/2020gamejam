﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Util
{
    public static bool Contains<T>(this T[] array, T element) where T: System.IEquatable<T>
    {
        foreach (T entry in array)
        {
            if (entry != null && entry.Equals(element) || element != null)
            {
                return true;
            }
        }
        return false;
    }

    public static Vector3 SwitchYWithZ(this Vector3 vector)
    {
        float y = vector.y;
        vector.y = vector.z;
        vector.z = y;
        return vector;
    }

    public static void DrawPoint(Vector3 point, Vector3 size, Color color, float duration)
    {
        size *= 0.5f;
        Debug.DrawLine(point + Vector3.up * size.y, point - Vector3.up * size.y, color, duration);
        Debug.DrawLine(point + Vector3.right * size.x, point - Vector3.right * size.x, color, duration);
        Debug.DrawLine(point + Vector3.forward * size.z, point - Vector3.forward * size.z, color, duration);

        size *= 0.5f;
        Debug.DrawLine(point + Vector3.up * size.y, point + Vector3.right * size.x, color, duration);
        Debug.DrawLine(point + Vector3.right * size.x, point - Vector3.up * size.y, color, duration);
        Debug.DrawLine(point - Vector3.up * size.y, point - Vector3.right * size.x, color, duration);
        Debug.DrawLine(point - Vector3.right * size.x, point + Vector3.up * size.y, color, duration);

        Debug.DrawLine(point + Vector3.up * size.y, point + Vector3.forward * size.z, color, duration);
        Debug.DrawLine(point + Vector3.forward * size.z, point - Vector3.up * size.y, color, duration);
        Debug.DrawLine(point - Vector3.up * size.y, point - Vector3.forward * size.z, color, duration);
        Debug.DrawLine(point - Vector3.forward * size.z, point + Vector3.up * size.y, color, duration);

        Debug.DrawLine(point + Vector3.forward * size.z, point + Vector3.right * size.x, color, duration);
        Debug.DrawLine(point + Vector3.right * size.x, point - Vector3.forward * size.z, color);
        Debug.DrawLine(point - Vector3.forward * size.z, point - Vector3.right * size.x, color, duration);
        Debug.DrawLine(point - Vector3.right * size.x, point + Vector3.forward * size.z, color, duration);
    }
}
