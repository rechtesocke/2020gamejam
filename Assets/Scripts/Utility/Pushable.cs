﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    [RequireComponent(typeof(Rigidbody))]
    public class Pushable : MonoBehaviour
    {
        private FixedJoint joint;
        private BoxCollider box;


        [Space, SerializeField]
        private UnityEvent onPushStart = default;

        public event UnityAction OnPushStart
        {
            add => onPushStart.AddListener(value);
            remove => onPushStart.RemoveListener(value);
        }

        [SerializeField]
        private UnityEvent onPushEnd = default;

        public event UnityAction OnPushEnd
        {
            add => onPushEnd.AddListener(value);
            remove => onPushEnd.RemoveListener(value);
        }

        public bool IsPushed => joint;


        private void Awake()
        {
            box = GetComponent<BoxCollider>();
        }

        public void Push(PushController controller)
        {
            if (!IsPushed)
            {
                Vector2 deltaPosition = (controller.transform.position - transform.position).SwitchYWithZ();
                if (Mathf.Abs(deltaPosition.x) > Mathf.Abs(deltaPosition.y))
                {
                    deltaPosition.y = 0f;
                }
                else
                {
                    deltaPosition.x = 0f;
                }
                deltaPosition = deltaPosition.normalized;
                controller.transform.forward = -((Vector3)deltaPosition).SwitchYWithZ();
                deltaPosition = box.size * deltaPosition;
                controller.transform.position = transform.position + ((Vector3)deltaPosition).SwitchYWithZ();

                joint = gameObject.AddComponent<FixedJoint>();
                joint.connectedBody = controller.Body;
            }
        }

        public void Release()
        {
            if (IsPushed)
            {
                joint.connectedBody = null;
                Destroy(joint);
            }
        }
    }
}
