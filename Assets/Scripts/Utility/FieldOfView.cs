namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEditor;

    public class FieldOfView : MonoBehaviour
    {
        // --- | Inspector | ---------------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The center of the view.")]
        private Vector3 center = Vector3.zero;
        /// <summary>
        /// The center of the view.
        /// </summary>
        public Vector3 Center => center;

        [SerializeField, Tooltip("The max distance the field reaches.")]
        private float radius = 1f;
        /// <summary>
        /// The max distance the field reaches.
        /// </summary>
        public float Radius
        {
            get => radius;
            private set
            {
                if (value < 0f)
                {
                    radius = 0f;
                }
                else
                {
                    radius = value;
                }
            }
        }

        [SerializeField, Range(0f, 360f), Tooltip("The angle the field.")]
        private float angle = 120;

        /// <summary>
        /// The angle the field.
        /// </summary>
        public float Angle
        {
            get => angle;
            private set
            {
                if (value > 360f)
                {
                    angle = 360f;
                }
                else if (value < 0f)
                {
                    angle = 0f;
                }
                else
                {
                    angle = value;
                }
            }
        }

        [SerializeField, Tooltip("The layermask to check for obstacles.")]
        private LayerMask obstacleLayer = ~0;


        // --- | Variables & Properties | --------------------------------------------------------------------------------------

        private Collider collider;


        // --- | Methods | -----------------------------------------------------------------------------------------------------

        private void Awake()
        {
            collider = GetComponentInChildren<Collider>();
        }

#if (UNITY_EDITOR)
        private void OnDrawGizmosSelected()
        {
            if (angle > 0f)
            {
                Gizmos.color = Color.cyan;
                Handles.color = Color.cyan;
                if (angle < 360f)
                {

                    Gizmos.DrawRay(transform.position + center, Quaternion.AngleAxis(angle * 0.5f, transform.up) * transform.forward * radius);
                    Vector3 right = Quaternion.AngleAxis(-angle * 0.5f, transform.up) * transform.forward * radius;
                    Gizmos.DrawRay(transform.position + center, Quaternion.AngleAxis(-angle * 0.5f, transform.up) * transform.forward * radius);
                    Gizmos.DrawRay(transform.position + center, Quaternion.AngleAxis(angle * 0.5f, transform.right) * transform.forward * radius);
                    Vector3 up = Quaternion.AngleAxis(-angle * 0.5f, transform.right) * transform.forward * radius;
                    Gizmos.DrawRay(transform.position + center, up);

                    Handles.DrawWireDisc(transform.position + center + transform.forward * Mathf.Cos(angle * 0.5f * Mathf.Deg2Rad) * radius, transform.forward, Mathf.Sin(angle * 0.5f * Mathf.Deg2Rad) * radius);
                    Handles.DrawWireArc(transform.position + center, transform.right, up, angle, radius);
                    Handles.DrawWireArc(transform.position + center, transform.up, right, angle, radius);

                    if (angle > 180f)
                    {
                        Handles.DrawWireDisc(transform.position + center, transform.forward, radius);
                    }
                }
                else
                {
                    Handles.DrawWireDisc(transform.position + center, transform.forward, radius);
                    Handles.DrawWireDisc(transform.position + center, transform.right, radius);
                    Handles.DrawWireDisc(transform.position + center, transform.up, radius);
                }
            }
        }
#endif

        /// <summary>
        /// Gets all targets in the field of view.
        /// </summary>
        /// <typeparam name="T">The type of the target.</typeparam>
        /// <param name="targetLayers">The layers to check for the target.</param>
        /// <param name="filter">The filter to compare the targets with, can be null.</param>
        /// <returns>All targets in the area, fitting the filter.</returns>
        public T[] GetTargets<T>(LayerMask targetLayers, Filter<T> filter) where T : Component
        {
            List<T> targets = new List<T>();
            foreach (Collider collider in Physics.OverlapSphere(transform.position + center, radius, targetLayers))
            {
                T target;
                if (ComponentMaster.TryFindTargetComponent(collider.gameObject, out target) &&
                    IsColliderInSight(collider) && (filter == null || filter.Compare(target)))
                {
                    targets.Add(target);
                }
            }

            return targets.ToArray();
        }

        /// <summary>
        /// Checks if a point can be seen with this field.
        /// </summary>
        /// <param name="point">The point to check.</param>
        /// <returns>True if the point can be seen.</returns>
        public bool IsPointInSight(Vector3 point)
        {
            Vector3 targetDir = point - transform.position + center;
            if (angle >= 360f || Mathf.Abs(Vector3.Angle(transform.forward, targetDir)) <= angle * 0.5f)
            {
                RaycastHit[] hits = Physics.RaycastAll(transform.position + center, targetDir.normalized, targetDir.magnitude, obstacleLayer, QueryTriggerInteraction.Ignore);
                if (hits.Length == 0)
                {
                    return true;
                }
                else if (hits.Length == 1)
                {
                    return hits[0].collider == collider;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks if a collider is inside the field.
        /// </summary>
        /// <param name="collider">The collider to check.</param>
        /// <returns>True if the collider can be seen.</returns>
        public bool IsColliderInSight(Collider collider)
        {
            Vector3 targetDir = collider.bounds.center - transform.position + center;
            if (angle >= 360f || Vector3.Angle(transform.forward, targetDir) <= angle * 0.5f)
            {
                RaycastHit[] hits = Physics.RaycastAll(transform.position + center, targetDir.normalized, targetDir.magnitude, obstacleLayer, QueryTriggerInteraction.Ignore);
                if (hits.Length <= 2)
                {
                    foreach (RaycastHit hit in hits)
                    {
                        if (hit.collider != collider)
                        {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return false;
        }


        // --- | Classes | -----------------------------------------------------------------------------------------------------

        /// <summary>
        /// A class to filter targets of a field of view.
        /// </summary>
        /// <typeparam name="T">The type of the component on the target.</typeparam>
        public abstract class Filter<T> where T : Component
        {
            /// <summary>
            /// Checks if the target fits the filter.
            /// </summary>
            /// <param name="target">The target to compare.</param>
            /// <returns>True if the target fits the filter.</returns>
            public abstract bool Compare(T target);
        }
    }
}
