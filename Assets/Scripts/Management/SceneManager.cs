﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.SceneManagement;

    public class SceneManager : MonoBehaviour
    {
        [SerializeField]
        private string[] sceneNames = default;


        private void Awake()
        {
            foreach (string scene in sceneNames)
            {
                if (!SceneCheck(scene))
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene(scene, LoadSceneMode.Additive);
                }
            }
        }

        private bool SceneCheck(string scene)
        {
            for (int i = 0; i < UnityEngine.SceneManagement.SceneManager.sceneCount; i++)
            {
                if (UnityEngine.SceneManagement.SceneManager.GetSceneAt(i).name == scene)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
