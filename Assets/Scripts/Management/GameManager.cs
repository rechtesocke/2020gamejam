﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class GameManager : MonoBehaviour
    {
        [SerializeField]
        private Bunny bunny = null;

        public Bunny Bunny => bunny;

        [SerializeField]
        private UnityEvent onGameOver = default;


        private void Start()
        {
            TimeKeeper.StartTracking();
        }

        public void CheckRevive()
        {
            if (bunny.IsAlive)
            {
                bunny.Revive();
            }
            else
            {
                GameOver();
            }
        }

        public void GameOver()
        {
            onGameOver?.Invoke();
        }
    }
}