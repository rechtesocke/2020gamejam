﻿namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// Manages <see cref="Component"/>s for quicker access.
    /// </summary>
    /// Contains methods to manage <see cref="Component"/>s.
    /// <list type="bullet">
    /// <item>
    /// <term>Register</term>
    /// <description>Registers components.</description>
    /// </item>
    /// <item>
    /// <term>TryFindTargetCompnent</term>
    /// <description>Returns registed components.</description>
    /// </item>
    /// </list>
    public static class ComponentMaster
    {
        // --- | Variables & Properties | ----------------------------------------------------------------------------------

        private static Dictionary<int, List<Component>> targets = new Dictionary<int, List<Component>>();


        // --- | Methods | -------------------------------------------------------------------------------------------------
        // Targets --------------------------------------------------------------------------------

        /// <summary>
        /// Register a <see cref="Component"/>.
        /// </summary>
        /// <param name="gameObject">The game object of the component.</param>
        /// <param name="component">The component to register.</param>
        /// <seealso cref="TryFindTargetComponent{T}(GameObject, out T)"/>
        public static void Register(this GameObject gameObject, Component component)
        {
            // Cash the id of the game object.
            int id = gameObject.GetInstanceID();
            // Check if the game object has already registered a component.
            if (!targets.ContainsKey(id))
            {
                // Add a new List of components for the game object.
                targets.Add(id, new List<Component>());
            }
            // Add the component to the list referenced by the game objects id.
            targets[id].Add(component);
        }

        /// <summary>
        /// Looks for a <see cref="Component"/> registered on a <see cref="GameObject"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="Component"/>.</typeparam>
        /// <param name="gameObject">The game object referencing the <see cref="Component"/>.</param>
        /// <param name="component">The component on the target.</param>
        /// <returns>True if a <see cref="Component"/> of the given type was registed on the given <see cref="GameObject"/>.</returns>
        /// <seealso cref="Register(GameObject, Component)"/>
        public static bool TryFindTargetComponent<T>(this GameObject gameObject, out T component) where T : Component
        {
            // Iterate over all components registed on the game object.
            foreach (Component element in DoFindComponents(gameObject, typeof(T)))
            {
                component = (T)element;
                return true;
            }
            // Set the component to null and return false, the component wasn't found.
            component = null;
            return false;
        }

        /// <summary>
        /// Looks for <see cref="Component"/>s registered on a <see cref="GameObject"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="Component"/>.</typeparam>
        /// <param name="gameObject">The game object referencing the <see cref="Component"/>.</param>
        /// <returns>The <see cref="Component"/>s found on the <see cref="GameObject"/>.</returns>
        public static T[] TryFindTargetComponents<T>(this GameObject gameObject) where T : Component
        {
            List<T> components = new List<T>();
            // Iterate over all components registed on the game object.
            foreach (Component element in DoFindComponents(gameObject, typeof(T)))
            {
                components.Add((T)element);
            }
            return components.ToArray();
        }

        /// <summary>
        /// Looks for <see cref="Component"/>s registered on a <see cref="GameObject"/> without allocating memory.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="Component"/>.</typeparam>
        /// <param name="gameObject">The game object referencing the <see cref="Component"/>.</param>
        /// <param name="components">The array for the components.</param>
        /// <returns>The amount of <see cref="Component"/>s found no the <see cref="GameObject"/></returns>
        public static int TryFindTargetComponentsAlloc<T>(this GameObject gameObject, T[] components) where T : Component
        {
            int count = 0;
            // Iterate over all components registed on the game object.
            foreach (Component element in DoFindComponents(gameObject, typeof(T)))
            {
                components[count] = ((T)element);
                count++;
            }
            return count;
        }

        /// <summary>
        /// Iterates over all <see cref="Component"/>s registert to the <paramref name="gameObject"/> and returns the ones of the <paramref name="type"/>.
        /// </summary>
        /// <param name="gameObject">The game object to check for the <see cref="Component"/>s.</param>
        /// <param name="type">The type of the <see cref="Component"/>s.</param>
        /// <returns>The <see cref="Component"/>s of the type.</returns>
        private static IEnumerable<Component> DoFindComponents(GameObject gameObject, System.Type type)
        {
            // Get the game objects id.
            int id = gameObject.GetInstanceID();
            // Check if the game object with the id is regeistered.
            if (targets.ContainsKey(id))
            {
                // Iterate over all components registed on the game object.
                foreach (Component element in targets[id])
                {
                    // Check if the type of the component is of the target type or inherits from it.
                    if (type.IsInstanceOfType(element))
                    {
                        yield return element;
                    }
                }
            }
        }
    }
}
