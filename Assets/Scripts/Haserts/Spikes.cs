﻿namespace GameJam2020
{
    using UnityEngine;

    public class Spikes : MonoBehaviour
    {
        private int frame = 0;
        private void OnTriggerEnter(Collider other)
        {
            if (Time.frameCount != frame)
            {
                LivingEntity entity = other.GetComponent<LivingEntity>();
                if (entity)
                {
                    entity.Die();
                }
                frame = Time.frameCount;
            }
        }
    }
}
