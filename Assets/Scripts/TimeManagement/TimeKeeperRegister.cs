﻿namespace GameJam2020
{
    using UnityEngine;

    public class TimeKeeperRegister : MonoBehaviour
    {
        [SerializeField]
        private TimeTracker tracker = null;
        [SerializeField]
        private TimeTracer tracer = null;


        private void Awake()
        {
            TimeKeeper.AddTracking(tracker, tracer);
        }
    }
}
