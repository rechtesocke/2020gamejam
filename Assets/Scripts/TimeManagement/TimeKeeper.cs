﻿namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public static class TimeKeeper
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        private static List<TrackPair> trackPairs = new List<TrackPair>();

        private static float tracingTimeScale = 1f;

        public static float TraceTimeScale => tracingTimeScale;

        public static bool IsTracing { get; private set; }

        public static event UnityAction OnTrackingStart = default;
        public static event UnityAction OnTrackingEnd = default;
        public static event UnityAction OnTracingStart = default;
        public static event UnityAction OnTracingEnd = default;

        private static int traceCount = 0;
        private static bool isUpdatingTraceCount = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public static void AddTracking(TimeTracker tracker, TimeTracer tracer)
        {
            trackPairs.Add(new TrackPair(tracker, tracer));
            tracer.OnTraceEnd += EndTrack;
        }

        public static void StartTracking()
        {
            foreach (TrackPair pair in trackPairs)
            {
                pair.StartTracking();
            }
            OnTrackingStart?.Invoke();
        }

        public static void StopTracking()
        {
            foreach (TrackPair pair in trackPairs)
            {
                pair.StopTracker();
            }
            OnTrackingEnd?.Invoke();
        }

        public static void StartTracing()
        {
            IsTracing = true;
            traceCount = 0;
            isUpdatingTraceCount = true;
            float startTime = FindStartTime();
            foreach (TrackPair pair in trackPairs)
            {
                if (pair.NodeCount > 0)
                {
                    pair.StartTracing(startTime, tracingTimeScale);
                    traceCount++;
                }
            }
            isUpdatingTraceCount = false;
            CheckTrackCount();
            OnTracingStart?.Invoke();
        }

        public static void StopTracing()
        {
            foreach (TrackPair pair in trackPairs)
            {
                if (pair.NodeCount > 0)
                {
                    pair.StopTracing();
                }
            }
            CheckTrackCount();
        }

        private static void EndTrack()
        {
            traceCount--;
            CheckTrackCount();
        }

        private static void CheckTrackCount()
        {
            if (traceCount <= 0 && !isUpdatingTraceCount)
            {
                IsTracing = false;
                OnTracingEnd?.Invoke();
            }
        }

        private static float FindStartTime()
        {
            float startTime = float.PositiveInfinity;
            foreach (TrackPair pair in trackPairs)
            {
                if (pair.NodeCount > 0)
                {
                    startTime = Mathf.Min(startTime, pair.StartTime);
                }
            }
            return startTime;
        }

        public static void SetTraceTimeScale(float timeScale)
        {
            tracingTimeScale = timeScale;
        }


        // --- | Classes | -------------------------------------------------------------------------------------------------

        [System.Serializable]
        private class TrackPair
        {
            [SerializeField]
            protected TimeTracker tracker = null;

            public TimeTracker Tracker => tracker;

            [SerializeField]
            protected TimeTracer tracer = null;

            public TimeTracer Tracer => tracer;

            private Queue<TimeNode> track;

            public int NodeCount => track == null ? 0 : track.Count;
            public float StartTime => track.Peek().Time;


            public TrackPair(TimeTracker tracker, TimeTracer tracer)
            {
                this.tracker = tracker;
                this.tracer = tracer;
            }


            public void StartTracking() => tracker.StartTracking();

            public void StopTracker()
            {
                track = tracker.StopTracker();
            }

            public void StartTracing(float startTime, float timeScale) => tracer.StartTracing(startTime, timeScale, track);

            public void StopTracing() => tracer.StopTracing();
        }
    }
}
