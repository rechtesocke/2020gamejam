﻿namespace GameJam2020
{
    using UnityEngine;

    public class TimeTransformNode : TimeNode
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        public Vector3 Position { get; private set; }
        public Vector3 EulerAngles { get; private set; }


        // --- | Constructor | ---------------------------------------------------------------------------------------------

        public TimeTransformNode(Transform target)
        {
            Position = target.position;
            EulerAngles = target.eulerAngles;
        }

        public TimeTransformNode(float time, Vector3 position, Vector3 eulerAngles) : base()
        {
            Time = time;
            Position = position;
            EulerAngles = eulerAngles;
        }

        // --- | Methods | -------------------------------------------------------------------------------------------------

        public static TimeTransformNode GetDelta(TimeTransformNode next, TimeTransformNode prev)
        {
            return new TimeTransformNode(next.Time - prev.Time, next.Position - prev.Position, Vector3.up * Mathf.DeltaAngle(prev.EulerAngles.y, next.EulerAngles.y));
        }
    }
}