﻿namespace GameJam2020
{
    public abstract class TimeNode
    {
        public float Time { get; protected set; }

        public TimeNode()
        {
            Time = UnityEngine.Time.time;
        }
    }
}
