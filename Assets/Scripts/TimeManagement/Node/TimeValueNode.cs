﻿using System;

namespace GameJam2020
{
    public class TimeValueNode : TimeNode
    {
        public string Name { get; protected set; }
        public object Value { get; protected set; }
        private ITrackable Trackable { get; set; }

        public TimeValueNode(ITrackable trackable, string name, object value) : base()
        {
            Trackable = trackable;
            Name = name ?? throw new ArgumentNullException(nameof(name));
            Value = value;
        }

        public void Recreate()
        {
            Trackable.Recreate(Name, Value);
        }
    }
}
