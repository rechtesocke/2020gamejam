﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class TimeKeeperReference : MonoBehaviour
    {
        [SerializeField, Min(0f)]
        private float traceTimeScale = 1f;

        [SerializeField]
        private UnityEvent onTrackerStart = default;
        [SerializeField]
        private UnityEvent onTrackerEnd = default;
        [SerializeField]
        private UnityEvent onTracerStart = default;
        [SerializeField]
        private UnityEvent onTracerEnd = default;

        private void Awake()
        {
            UpdateTimeScale();
            TimeKeeper.OnTrackingStart += onTrackerStart.Invoke;
            TimeKeeper.OnTrackingEnd += onTrackerEnd.Invoke;
            TimeKeeper.OnTracingStart += onTracerStart.Invoke;
            TimeKeeper.OnTracingEnd += onTracerEnd.Invoke;
        }

        [ContextMenu("Update time scale")]
        public void UpdateTimeScale()
        {
            TimeKeeper.SetTraceTimeScale(traceTimeScale);
        }

        public void StartTracking() => TimeKeeper.StartTracking();

        public void StopTracking() => TimeKeeper.StopTracking();

        public void StartTracing() => TimeKeeper.StartTracing();

        public void StopTracing() => TimeKeeper.StopTracing();
    }
}
