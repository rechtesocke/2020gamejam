﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public interface ITrackable
    {
        void StartTracking();
        void StopTracker();
        void Recreate(string name, object value);
    }
}
