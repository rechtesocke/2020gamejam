﻿namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class TimeTracer : MonoBehaviour
    {
        [SerializeField]
        protected UnityEvent onTracingStart = default;

        [SerializeField]
        protected UnityEvent onTracingEnd = default;
        public event UnityAction OnTraceEnd
        {
            add => onTracingEnd.AddListener(value);
            remove => onTracingEnd.RemoveListener(value);
        }


        public abstract bool IsActive { get; }

        public abstract void StartTracing(float startTime, float timeScale, Queue<TimeNode> track);

        public abstract void StopTracing();
    }
}
