﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public class TimeTransformTracer : TimeTracer
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField]
        protected Rigidbody target = null;

        [SerializeField]
        private UnityEvent onBlockedPath = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Coroutine trace = null;
        public override bool IsActive => trace != null || started;
        private bool started = false;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public override void StartTracing(float startTime, float timeScale, Queue<TimeNode> track)
        {
            if (!IsActive)
            {
                started = true;
                trace = StartCoroutine(DoRetrace(startTime, timeScale, track));
                started = false;
            }
        }

        public override void StopTracing()
        {
            if (IsActive)
            {
                StopCoroutine(trace);
                trace = null;
                onTracingEnd?.Invoke();
            }
        }

        protected IEnumerator DoRetrace(float startTime, float timeScale, Queue<TimeNode> track)
        {
            TimeTransformNode prevNode = (TimeTransformNode)track.Dequeue();
            float time = prevNode.Time;
            float deltaTime = (time - startTime) / timeScale;
            if (deltaTime > 0f)
            {
                yield return new WaitForSeconds(deltaTime);
            }

            target.transform.position = prevNode.Position;
            target.transform.eulerAngles = prevNode.EulerAngles;
            onTracingStart?.Invoke();
            while (track.Count > 0)
            {
                TimeTransformNode nextNode = (TimeTransformNode)track.Dequeue();
                TimeTransformNode deltaNode = TimeTransformNode.GetDelta(nextNode, prevNode);

                while (time < nextNode.Time)
                {
                    yield return new WaitForFixedUpdate();
                    time += Time.fixedDeltaTime * timeScale;
                    float progress = (time - prevNode.Time) / deltaNode.Time;
                    Vector3 targetPos = prevNode.Position + deltaNode.Position * progress;
                    if (CheckPath(targetPos - target.transform.position))
                    {
                        onBlockedPath?.Invoke();
                        StopTracing();

                        if (!IsActive)
                        {
                            break;
                        }
                    }
                    target.transform.position = targetPos;
                    target.transform.eulerAngles = prevNode.EulerAngles + deltaNode.EulerAngles * progress;
                }
                prevNode = nextNode;
                if (!IsActive)
                {
                    break;
                }
            }
            if (IsActive)
            {
                trace = null;
                onTracingEnd?.Invoke();
            }
        }

        private bool CheckPath(Vector3 direction)
        {
            if (target.SweepTest(direction, out RaycastHit hit, direction.magnitude, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(hit.normal.y) < 0.2f)
                {
                    if (hit.point.y != hit.collider.bounds.min.y && hit.point.y != hit.collider.bounds.max.y)
                    { 
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
