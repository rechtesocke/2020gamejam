﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TimeValueTracer : TimeTracer
    {
        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Coroutine trace = null;
        public override bool IsActive => trace != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public override void StartTracing(float startTime, float timeScale, Queue<TimeNode> track)
        {
            if (!IsActive)
            {
                StartCoroutine(DoTrace(startTime, timeScale, track));
            }
        }

        public override void StopTracing()
        {
            if (IsActive)
            {
                StopCoroutine(trace);
                trace = null;
                onTracingEnd?.Invoke();
            }
        }

        private IEnumerator DoTrace(float startTime, float timeScale, Queue<TimeNode> track)
        {
            TimeValueNode node = (TimeValueNode)track.Dequeue();

            float time = node.Time;
            float deltaTime = (time - startTime) / timeScale;
            if (deltaTime > 0f)
            {
                yield return new WaitForSeconds(deltaTime);
            }

            onTracingStart?.Invoke();
            while (true)
            {
                time = node.Time;
                node.Recreate();
                if (track.Count == 0)
                {
                    break;
                }
                node = (TimeValueNode)track.Dequeue();
                yield return new WaitForSeconds((node.Time - time) / timeScale);
            }
            trace = null;
            onTracingEnd?.Invoke();
        }
    }
}
