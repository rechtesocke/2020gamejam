﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TimeTransformTracker : TimeTracker
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField]
        private Transform target = null;

        [SerializeField]
        private float timeSteps = 0.2f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Coroutine tracker = null;

        public override bool IsActive => tracker != null;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        public override void StartTracking()
        {
            if (!IsActive)
            {
                track.Clear();
                tracker = StartCoroutine(DoTrack());
            }
        }

        public void AddCurrentPosition()
        {
            if (IsActive)
            {
                track.Enqueue(new TimeTransformNode(target));
            }
        }

        public override Queue<TimeNode> StopTracker()
        {
            if (IsActive)
            {
                onTrackingEnd?.Invoke();
                StopCoroutine(tracker);
                track.Enqueue(new TimeTransformNode(target));
                tracker = null;
                return track;
            }
            return null;
        }

        private IEnumerator DoTrack()
        {
            TimeTransformNode prev = null;
            onTrackingStart?.Invoke();
            bool didSkip = false;
            while (true)
            {
                TimeTransformNode next = new TimeTransformNode(target);
                if (prev == null || (prev.Position != next.Position || prev.EulerAngles != next.EulerAngles))
                {
                    if (didSkip)
                    {
                        track.Enqueue(prev);
                        didSkip = false;
                    }
                    track.Enqueue(next);
                }
                else
                {
                    didSkip = true;
                }
                prev = next;
                yield return new WaitForSeconds(timeSteps);
            }
        }
    }
}
