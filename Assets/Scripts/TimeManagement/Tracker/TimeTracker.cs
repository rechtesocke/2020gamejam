﻿namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class TimeTracker : MonoBehaviour
    {
        [Space, SerializeField]
        protected UnityEvent onTrackingStart = default;

        [SerializeField]
        protected UnityEvent onTrackingEnd = default;


        protected Queue<TimeNode> track = new Queue<TimeNode>();

        public abstract bool IsActive { get; }

        public abstract void StartTracking();

        public abstract Queue<TimeNode> StopTracker();
    }
}
