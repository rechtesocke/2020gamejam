﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class TimeValueTracker : TimeTracker
    {
        public static TimeValueTracker Instance { get; private set; }

        private bool isActive = false;
        public override bool IsActive => isActive;

        protected List<ITrackable> trackables;

        private void Awake()
        {
            if (!Instance)
            {
                Instance = this;
            }
            trackables = new List<ITrackable>();
        }

        public void RegisterTrackable(ITrackable trackable)
        {
            trackables.Add(trackable);
            if (IsActive)
            {
                trackable.StartTracking();
            }
        }

        public void TrackValue(TimeNode value)
        {
            if (isActive)
            {
                track.Enqueue(value);
            }
        }

        public override void StartTracking()
        {
            if (!IsActive)
            {
                isActive = true;
                track.Clear();
                foreach (ITrackable trackable in trackables)
                {
                    trackable.StartTracking();
                }
            }
        }

        public override Queue<TimeNode> StopTracker()
        {
            if (IsActive)
            {
                isActive = false;
                foreach (ITrackable trackable in trackables)
                {
                    trackable.StopTracker();
                }
                return track;
            }
            return null;
        }
    }
}
