﻿namespace GameJam2020
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Events;

    public class EnemyController : MonoBehaviour, ITrackable, IDistrectable
    {
        [SerializeField]
        private FieldOfView view = null;

        [SerializeField]
        private LayerMask targetLayers = ~0;

        [SerializeField]
        private float rotationSpeed = 20f;

        [SerializeField, Range(0f, 180f)]
        private float attackAngle = 10f;

        [SerializeField]
        private float chargeDuration = 1f;

        [SerializeField]
        private UnityEvent onCharged = default;


        private Bunny target = null;
        private float lastTargetDirection = 0f;
        private bool looksAtTarget = true;
        private Coroutine charge = null;
        private bool isDistrected = false;

        public bool IsEngaged => target != null && !isDistrected;

        private float FixedDeltaTime => Time.fixedDeltaTime * (TimeKeeper.IsTracing ? TimeKeeper.TraceTimeScale : 1f);

        private void Start()
        {
            TimeValueTracker.Instance.RegisterTrackable(this);
        }

        private void Update()
        {
            if (!isDistrected)
            {
                Bunny[] bunnys = view.GetTargets<Bunny>(targetLayers, null);
                if (IsEngaged)
                {
                    if (!bunnys.Contains(target))
                    {
                        target = null;
                    }
                    else
                    {
                        lastTargetDirection = Vector2.SignedAngle((target.transform.position - transform.position).SwitchYWithZ(), Vector2.up);
                    }
                }
                else if (bunnys.Length > 0)
                {
                    target = bunnys[0];
                    lastTargetDirection = Vector2.SignedAngle((target.transform.position - transform.position).SwitchYWithZ(), Vector2.up);
                }
            }
        }

        private void FixedUpdate()
        {
            if (IsEngaged || isDistrected || !looksAtTarget)
            {
                Vector3 angles = transform.eulerAngles;
                angles.y = Mathf.MoveTowardsAngle(angles.y, lastTargetDirection, rotationSpeed * FixedDeltaTime);
                transform.eulerAngles = angles;
                looksAtTarget = angles.y == lastTargetDirection;
                if (IsEngaged)
                {
                    if (IsEngaged && Mathf.Abs(Mathf.DeltaAngle(transform.eulerAngles.y, lastTargetDirection)) <= attackAngle * 0.5f)
                    {
                        if (charge == null)
                        {
                            charge = StartCoroutine(DoCharge());
                        }
                    }
                    else if (charge != null)
                    {
                        StopCoroutine(charge);
                        charge = null;
                    }
                }
            }
        }

        private IEnumerator DoCharge()
        {
            yield return new WaitForSeconds(chargeDuration / (TimeKeeper.IsTracing ? TimeKeeper.TraceTimeScale : 1f));
            charge = null;
            target.Die();
            target = null;
            onCharged?.Invoke();
        }

        public void StartDistrection(Vector3 position)
        {
            isDistrected = true;
            lastTargetDirection = Vector2.SignedAngle((position - transform.position).SwitchYWithZ(), Vector2.up);
        }

        public void StopDistrection()
        {
            isDistrected = false;
        }

        public void StartTracking()
        {
            TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, nameof(transform.eulerAngles), transform.eulerAngles));
            TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, "ENEMY_VALUES", new TrackedValues(this)));
        }

        public void StopTracker() { }

        public void Recreate(string name, object value)
        {
            if (charge != null)
            {
                StopCoroutine(charge);
                charge = null;
            }
            switch (name)
            {
                case nameof(transform.eulerAngles):
                    transform.eulerAngles = (Vector3)value;
                    break;
                case "ENEMY_VALUES":
                    ((TrackedValues)value).Apply(this);
                    break;
            }
        }

        private class TrackedValues
        {
            public Bunny target = null;
            public float lastTargetDirection = 0f;
            public bool looksAtTarget = true;
            public bool isDistrected = false;

            public TrackedValues(EnemyController enemy)
            {
                target = enemy.target;
                lastTargetDirection = enemy.lastTargetDirection;
                looksAtTarget = enemy.looksAtTarget;
                isDistrected = enemy.isDistrected;
            }

            public void Apply(EnemyController enemy)
            {
                enemy.target = target;
                enemy.lastTargetDirection = lastTargetDirection;
                enemy.looksAtTarget = looksAtTarget;
                enemy.isDistrected = isDistrected;
            }
        }
    }
}
