﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class Highlighter : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onHighlightStart = default;

        [SerializeField]
        private UnityEvent onHighlightEnd = default;

        public static Highlighter Active { get; private set; } = null;

        public static void Highlight(Highlighter highlighter)
        {
            if (highlighter != Active)
            {
                Active?.onHighlightEnd?.Invoke();
                Active = highlighter;
                Active?.onHighlightStart?.Invoke();
            }
        }
    }
}
