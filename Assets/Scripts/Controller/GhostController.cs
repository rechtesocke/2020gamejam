﻿namespace GameJam2020
{
    using UnityEngine;

    [RequireComponent(typeof(Rigidbody))]
    public class GhostController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [Header("Values")]
        [SerializeField, Min(0f)]
        private float speed = 2f;

        [SerializeField, Min(0f)]
        private float ghostAdjustSpeed = 1f;

        [SerializeField, Min(0f)]
        private float leanStrenght = 500f;

        [Header("References")]
        [SerializeField]
        private Transform cameraController = null;

        [SerializeField]
        private Transform ghost = null;

        [SerializeField]
        private Transform bunny = null;

        [SerializeField]
        private Vector3 offset = new Vector3(0f, 2f, 0f);


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private Rigidbody body;
        private float targetRotation;


        // --- | MEthods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            body = GetComponent<Rigidbody>();
            targetRotation = cameraController.eulerAngles.y;
        }

        private void FixedUpdate()
        {
            float verticalMove = 0f;
            if (Input.GetKey(KeyCode.Space))
            {
                verticalMove++;
            }
            if (Input.GetKey(KeyCode.LeftShift))
            {
                verticalMove--;
            }

            body.velocity = (cameraController.right * Input.GetAxisRaw("Horizontal") + cameraController.forward * Input.GetAxisRaw("Vertical") + cameraController.up * verticalMove) * speed;
        }

        private void Update()
        {
            Vector3 ghostAngles = ghost.eulerAngles;
            float stepAdjustSpeed = (Time.deltaTime / ghostAdjustSpeed);
            if (body.velocity.sqrMagnitude > 0f)
            {
                targetRotation = cameraController.eulerAngles.y;
            }
            Vector3 invTransform = ghost.InverseTransformVector(body.velocity) * leanStrenght;
            ghostAngles.x += Mathf.DeltaAngle(ghostAngles.x, invTransform.z) * stepAdjustSpeed;
            ghostAngles.y += Mathf.DeltaAngle(ghostAngles.y, targetRotation) * stepAdjustSpeed;
            ghostAngles.z += Mathf.DeltaAngle(ghostAngles.z, -invTransform.x) * stepAdjustSpeed;
            ghost.eulerAngles = ghostAngles;
        }
    }
}
