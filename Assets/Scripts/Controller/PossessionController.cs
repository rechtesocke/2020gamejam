﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class PossessionController : MonoBehaviour
    {
        [SerializeField]
        protected new Camera camera = null;

        [SerializeField]
        private UnityEvent onPossessed = default;

        [SerializeField]
        private UnityEvent onLeft = default;

        [SerializeField]
        private float possessionRange = 5f;

        [SerializeField]
        private LayerMask targetLayers = ~0;

        [SerializeField]
        protected Highlighter highlighter = null;

        public Highlighter Highlighter => highlighter;

        protected PossessionController previous;
        protected PossessionController next = null;


        private void Update()
        {
            RaycastHit[] hits = Physics.RaycastAll(camera.transform.position, camera.transform.forward, 5f, targetLayers);
            for (int i = 0; i < hits.Length && i < 2; i++)
            {
                if (hits[i].transform == transform)
                {
                    continue;
                }
                PossessionController possessable = hits[i].transform.GetComponent<PossessionController>();
                if (possessable)
                {
                    Highlighter.Highlight(possessable.Highlighter);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        if (possessable.next != null)
                        {
                            Return(possessable);
                        }
                        else
                        { 
                            onLeft?.Invoke();
                            next = possessable;
                            possessable.GetPossessed(this);
                            enabled = false;
                        }
                    }
                    break;
                }
                else
                {
                    Highlighter.Highlight(null);
                }
            }
            if (hits.Length == 0)
            {
                Highlighter.Highlight(null);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Return();
            }
        }

        public void GetPossessed(PossessionController previous)
        {
            enabled = true;
            this.previous = previous;
            onPossessed?.Invoke();
        }

        private void Return(PossessionController target)
        {
            if (next)
            {
                next.Return(target);
            }
            else if (previous == target || previous == null)
            {
                Highlighter.Highlight(null);
                enabled = true;
                onPossessed?.Invoke();
            }
            else
            {
                previous.next = null;
                if (enabled)
                {
                    enabled = false;
                    onLeft?.Invoke();
                }
                previous.Return(target);
                previous = null;
            }
        }
        public void Return() => Return(null);
    }
}
