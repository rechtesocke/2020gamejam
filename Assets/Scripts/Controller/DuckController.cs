﻿namespace GameJam2020
{
    using UnityEngine;

    public class DuckController : MonoBehaviour, ITrackable
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField]
        private float duckHeight = 1f;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private CapsuleCollider attachedCollider;
        private float standingHeight;
        private Vector3 duckedCenter;
        private Vector3 standingCenter;
        private bool isDucked = false;
        private bool shouldStandUp = true;
        private bool isRecording = false;


        // --- | MEthods | -------------------------------------------------------------------------------------------------

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            CapsuleCollider collider = attachedCollider ? attachedCollider : GetComponent<CapsuleCollider>();

            UnityEditor.Handles.color = Color.blue;

            Vector3 bottom = transform.position + collider.center - transform.up * (collider.height * 0.5f - collider.radius);
            Vector3 top = transform.position + collider.center - transform.up * (collider.height * 0.5f + collider.radius - (isDucked ? standingHeight : duckHeight));

            UnityEditor.Handles.DrawWireArc(bottom, transform.forward, -transform.right, 180f, collider.radius);
            UnityEditor.Handles.DrawWireArc(bottom, transform.right, transform.forward, 180f, collider.radius);

            UnityEditor.Handles.DrawWireDisc(bottom, transform.up, collider.radius);

            if (bottom != top)
            {
                UnityEditor.Handles.DrawWireDisc(top, transform.up, collider.radius);
                UnityEditor.Handles.DrawLine(top + transform.right * collider.radius, bottom + transform.right * collider.radius);
                UnityEditor.Handles.DrawLine(top - transform.right * collider.radius, bottom - transform.right * collider.radius);
                UnityEditor.Handles.DrawLine(top + transform.forward * collider.radius, bottom + transform.forward * collider.radius);
                UnityEditor.Handles.DrawLine(top - transform.forward * collider.radius, bottom - transform.forward * collider.radius);
            }

            UnityEditor.Handles.DrawWireArc(top, transform.forward, transform.right, 180f, collider.radius);
            UnityEditor.Handles.DrawWireArc(top, transform.right, -transform.forward, 180f, collider.radius);
        }

#endif

        private void Awake()
        {
            attachedCollider = GetComponent<CapsuleCollider>();
            standingHeight = attachedCollider.height;
            standingCenter = attachedCollider.center;
            duckedCenter = standingCenter - (transform.up * (standingHeight - duckHeight)) * 0.5f;
        }

        private void Start()
        {
            TimeValueTracker.Instance.RegisterTrackable(this);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                Duck();
            }
            if (isDucked && (shouldStandUp || Input.GetKeyUp(KeyCode.LeftShift)))
            {
                StandUp();
            }
        }

        public void Duck()
        {
            if (!isDucked)
            {
                isDucked = true;
                shouldStandUp = false;
                attachedCollider.height = duckHeight;
                attachedCollider.center = duckedCenter;
                if (isRecording)
                {
                    TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, nameof(isDucked), isDucked));
                }
            }
        }

        public void StandUp()
        {
            if (isDucked)
            {
                shouldStandUp = true;
                if (Physics.SphereCastAll(new Ray(transform.position, transform.up), attachedCollider.radius, standingHeight).Length == 1)
                {
                    isDucked = false;
                    attachedCollider.height = standingHeight;
                    attachedCollider.center = standingCenter;
                    if (isRecording)
                    {
                        TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, nameof(isDucked), isDucked));
                    }
                }
            }
        }

        public void StartTracking()
        {
            isRecording = true;
            TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, nameof(isDucked), isDucked));
        }

        public void StopTracker()
        {
            isRecording = false;
        }

        public void Recreate(string name, object value)
        {
            switch (name)
            {
                case nameof(isDucked):
                    if ((bool)value)
                    {
                        Duck();
                    }
                    else
                    {
                        StandUp();
                    }
                    break;
            }
        }
    }
}
