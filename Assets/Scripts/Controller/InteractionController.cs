﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class InteractionController : MonoBehaviour
    {
        [SerializeField]
        protected new Camera camera = null;

        [SerializeField]
        private float interactionRange = 5f;

        [SerializeField]
        private LayerMask targetLayers = ~0;


        private void Update()
        {
            RaycastHit[] hits = Physics.RaycastAll(camera.transform.position, camera.transform.forward, 5f, targetLayers);
            for (int i = 0; i < hits.Length && i < 2; i++)
            {
                if (hits[i].transform == transform)
                {
                    continue;
                }
                IInteractable interactable = hits[i].transform.GetComponent<IInteractable>();
                if (interactable != null)
                {
                    Highlighter.Highlight(interactable.Highlighter);
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        interactable.Interact();
                    }
                    break;
                }
                else
                {
                    Highlighter.Highlight(null);
                }
            }
            if (hits.Length == 0)
            {
                Highlighter.Highlight(null);
            }
        }
    }
}
