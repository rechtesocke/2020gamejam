﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class PushController : MonoBehaviour
    {
        [SerializeField]
        private Transform head = null;

        [SerializeField]
        private LayerMask pushableLayers = ~0;

        [Space, SerializeField]
        private UnityEvent onPushStart = default;

        [SerializeField]
        private UnityEvent onPushEnd = default;



        public Rigidbody Body { get; private set; }

        RaycastHit[] hitbuffer = new RaycastHit[2];
        public bool IsPushing => pushable != null;
        private Pushable pushable = null;


        private void Awake()
        {
            Body = GetComponent<Rigidbody>();
        }

        private void OnDisable()
        {
            StopPushing();
        }

        private void Update()
        {
            if (Input.GetAxisRaw("Vertical") > 0f)
            {
                int count = Physics.RaycastNonAlloc(new Ray(head.position, head.forward), hitbuffer, 1f, pushableLayers, QueryTriggerInteraction.Ignore);
                for (int i = 0; i < count; i++)
                {
                    if (hitbuffer[i].transform == transform)
                    {
                        continue;
                    }
                    Pushable pushable = hitbuffer[i].transform.GetComponent<Pushable>();
                    if (pushable)
                    {
                        StartPusing(pushable);
                    }
                    break;
                }
            }
        }

        private void FixedUpdate()
        {
            if (IsPushing && Input.GetAxisRaw("Vertical") < 0f)
            {
                StopPushing();
            }
        }

        private void StartPusing(Pushable pushable)
        {
            if (!IsPushing)
            {
                this.pushable = pushable;
                pushable.Push(this);
                onPushStart?.Invoke();
            }
        }

        public void StopPushing()
        {
            if (IsPushing)
            {
                pushable.Release();
                onPushEnd?.Invoke();
                pushable = null;
            }
        }
    }
}
