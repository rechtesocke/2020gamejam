﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class SimpleMovementController : MonoBehaviour
    {
        [SerializeField]
        private new Camera camera = null;

        [SerializeField]
        private float speed = 5f;

        [SerializeField]
        private UnityEvent onMoveStart = default;

        private Rigidbody body;


        private void Awake()
        {
            body = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            Vector2 direction = transform.InverseTransformDirection(camera.transform.forward).SwitchYWithZ();
            Vector3 forward = Vector3.zero;
            Vector3 right = Vector3.zero;
            if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
            {
                if (direction.y > 0f)
                {
                    forward = transform.forward;
                    right = transform.right;
                }
                else
                {
                    forward = -transform.forward;
                    right = -transform.right;
                }
            }
            else
            {
                if (direction.x > 0f)
                {
                    forward = transform.right;
                    right = -transform.forward;
                }
                else
                {
                    forward = -transform.right;
                    right = transform.forward;
                }
            }

            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            bool wasMoving = body.velocity.sqrMagnitude > 0f;

            if (Mathf.Abs(input.x) < Mathf.Abs(input.y))
            {
                body.velocity = forward * input.y * speed;
            }
            else
            {
                body.velocity = right * input.x * speed;
            }

            if (!wasMoving && body.velocity.sqrMagnitude > 0f)
            {
                onMoveStart?.Invoke();
            }
        }
    }
}
