﻿namespace GameJam2020
{
    using UnityEngine;

    public class GhostPosition : MonoBehaviour
    {
        [SerializeField]
        private Vector3 offset = Vector3.zero;

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Debug.DrawLine(transform.position + offset + transform.up * 0.25f, transform.position + offset - transform.up * 0.25f, Color.red);
            Debug.DrawLine(transform.position + offset + transform.right * 0.25f, transform.position + offset - transform.right * 0.25f, Color.red);
            Debug.DrawLine(transform.position + offset + transform.forward * 0.25f, transform.position + offset - transform.forward * 0.25f, Color.red);

            Debug.DrawLine(transform.position + offset + transform.up * 0.125f, transform.position + offset + transform.right * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset + transform.right * 0.125f, transform.position + offset - transform.up * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.up * 0.125f, transform.position + offset - transform.right * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.right * 0.125f, transform.position + offset + transform.up * 0.125f, Color.red);

            Debug.DrawLine(transform.position + offset + transform.up * 0.125f, transform.position + offset + transform.forward * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset + transform.forward * 0.125f, transform.position + offset - transform.up * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.up * 0.125f, transform.position + offset - transform.forward * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.forward * 0.125f, transform.position + offset + transform.up * 0.125f, Color.red);

            Debug.DrawLine(transform.position + offset + transform.forward * 0.125f, transform.position + offset + transform.right * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset + transform.right * 0.125f, transform.position + offset - transform.forward * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.forward * 0.125f, transform.position + offset - transform.right * 0.125f, Color.red);
            Debug.DrawLine(transform.position + offset - transform.right * 0.125f, transform.position + offset + transform.forward * 0.125f, Color.red);
        }

#endif


        public void MoveGhostTo(Transform ghost)
        {
            ghost.transform.position = transform.position + offset;
        }
    }
}
