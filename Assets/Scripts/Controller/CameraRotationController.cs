﻿namespace GameJam2020
{
    using UnityEngine;

    public class CameraRotationController : MonoBehaviour
    {
        [SerializeField]
        private Transform horizontalTarget = null;
        [SerializeField]
        private Transform verticalTarget = null;

        [SerializeField, Min(0f)]
        private float rotationSpeed = 10f;

        [SerializeField, Range(0f, 180f)]
        private float verticalRoationAngle = 180f;

        private bool isVerticalyLocked = false;
        private float lockedVerticalAngle = 0f;

        private void FixedUpdate()
        {
            float halfAngle = verticalRoationAngle * 0.5f;
            float horizontalAngle = horizontalTarget.localEulerAngles.x;
            horizontalAngle -= Input.GetAxisRaw("Mouse Y") * rotationSpeed;
            horizontalAngle = (horizontalAngle + 360f) % 360f;
            if (horizontalAngle > halfAngle && horizontalAngle < 180f)
            {
                horizontalAngle = halfAngle;
            }
            if (horizontalAngle < (360f - halfAngle) && horizontalAngle > 180f)
            {
                horizontalAngle = -halfAngle;
            }
            Vector3 angle = horizontalTarget.localEulerAngles;
            angle.x = horizontalAngle;
            horizontalTarget.localEulerAngles = angle;

            if (!isVerticalyLocked)
            {
                angle = verticalTarget.localEulerAngles;
                angle.y += Input.GetAxisRaw("Mouse X") * rotationSpeed;
                verticalTarget.localEulerAngles = angle;
            }
            else if (verticalTarget.localEulerAngles.y != lockedVerticalAngle)
            {
                angle = verticalTarget.localEulerAngles;
                angle.y = lockedVerticalAngle;
                verticalTarget.localEulerAngles = angle;
            }
        }

        public void LockVerticalRotation()
        {
            isVerticalyLocked = true;
            lockedVerticalAngle = verticalTarget.localEulerAngles.y;
        }

        public void UnlockVerticalRotation()
        {
            isVerticalyLocked = false;
        }
    }
}
