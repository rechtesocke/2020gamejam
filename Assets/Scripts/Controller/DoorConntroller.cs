﻿namespace GameJam2020
{
    using UnityEngine;

    public class DoorConntroller : MonoBehaviour
    {
        [SerializeField]
        private Door door = null;

        private float lateInput = 0f;

        private void OnEnable()
        {
            lateInput = door.IsOpen ? 1f : -1f;
        }

        private void Update()
        {
            float input = Input.GetAxisRaw("Vertical");
            if (input != 0f && (lateInput < 0f) != (input < 0f))
            {
                if (input > 0f)
                {
                    door.Open();
                }
                else
                {
                    door.Close();
                }
                lateInput = input;
            }
        }
    }
}
