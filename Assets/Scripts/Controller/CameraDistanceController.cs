﻿namespace GameJam2020
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraDistanceController : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField]
        private Transform target = null;

        [SerializeField]
        private new Camera camera = null;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        private float cameraDistance;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            cameraDistance = -camera.transform.localPosition.z;
        }

        private void LateUpdate()
        {
            if (Physics.Raycast(target.position, -target.forward, out RaycastHit hit, cameraDistance))
            {
                Vector3 cameraPos = camera.transform.localPosition;
                cameraPos.z = -hit.distance;
                camera.transform.localPosition = cameraPos;
            }
            else if (camera.transform.localPosition.z != -cameraDistance)
            {
                Vector3 cameraPos = camera.transform.localPosition;
                cameraPos.z = -cameraDistance;
                camera.transform.localPosition = cameraPos;
            }
        }
    }
}
