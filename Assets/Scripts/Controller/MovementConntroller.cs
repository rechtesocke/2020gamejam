namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    [RequireComponent(typeof(Rigidbody))]
    public class MovementConntroller : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Min(0f)]
        private float speed = 2f;

        [SerializeField, Min(0f)]
        private float turnSpeed = 10f;

        [Space, SerializeField]
        private UnityEvent onJump = default;

        [SerializeField]
        private UnityEvent onGrounded = default;
        [SerializeField]
        private UnityEvent onFall = default;
        [SerializeField]
        private UnityEvent onGroundedLeft = default;


        // --- | Variables | -----------------------------------------------------------------------------------------------

        public Rigidbody Body { get; private set; } = null;
        private bool forwardOnly = false;
        public bool IsGrounded => groundCount > 0;
        private bool isFalling = true;
        private int groundCount = 0;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        private void Awake()
        {
            Body = GetComponent<Rigidbody>();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void FixedUpdate()
        {
            if (forwardOnly)
            {
                Body.velocity = transform.forward * Mathf.Clamp01(Input.GetAxisRaw("Vertical")) * speed + Vector3.up * Body.velocity.y;
            }
            else
            {
                Body.velocity = (transform.right * Input.GetAxisRaw("Horizontal") + transform.forward * Input.GetAxisRaw("Vertical")) * speed + Vector3.up * Body.velocity.y;
            }
            if (!isFalling && Body.velocity.y < 0f)
            {
                isFalling = true;
                onFall?.Invoke();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && IsGrounded && !forwardOnly)
            {
                Vector3 velocity = Body.velocity;
                velocity.y = 10f;
                Body.velocity = velocity;
                isFalling = false;
                onJump?.Invoke();
            }
        }

        public void MoveForwardOnly()
        {
            forwardOnly = true;
        }

        public void MoveAllDirections()
        {
            forwardOnly = false;
        }

        private void OnTriggerEnter(Collider other)
        {
            groundCount++;
            if (groundCount == 1)
            {
                onGrounded?.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            groundCount--;
            if (!IsGrounded)
            { 
                onGroundedLeft?.Invoke();
            }
        }
    }
}
