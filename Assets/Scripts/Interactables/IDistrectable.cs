﻿namespace GameJam2020
{
    using UnityEngine;

    public interface IDistrectable
    {
        void StartDistrection(Vector3 position);
        void StopDistrection();
    }
}
