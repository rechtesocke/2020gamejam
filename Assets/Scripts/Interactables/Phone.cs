﻿namespace GameJam2020
{
    using System.Collections.Generic;
    using UnityEngine;

    public class Phone : MonoBehaviour
    {
        [SerializeField, Min(0f)]
        private float radius = 5f;

        [SerializeField]
        private Animator animator = null;

        private List<IDistrectable> targets = new List<IDistrectable>();

#if (UNITY_EDITOR)

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(transform.position, radius);
        }

#endif
        private void OnEnable()
        {
            animator.SetBool("Ring", true);
        }

        private void OnDisable()
        {
            animator.SetBool("Ring", false);
            foreach (IDistrectable target in targets)
            {
                target.StopDistrection();
            }
            targets.Clear();
        }


        private void Update()
        {
            List<IDistrectable> distrectables = new List<IDistrectable>();
            foreach (Collider collider in Physics.OverlapSphere(transform.position, radius))
            {
                IDistrectable distrectable = collider.GetComponent<IDistrectable>();
                if (distrectable != null)
                {
                    distrectables.Add(distrectable);
                    if (targets.Contains(distrectable))
                    {
                        targets.Remove(distrectable);
                    }
                    else
                    {
                        distrectable.StartDistrection(transform.position);
                    }
                }
            }
            foreach(IDistrectable target in targets)
            {
                target.StopDistrection();
            }
            targets = distrectables;
        }
    }
}
