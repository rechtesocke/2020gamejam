﻿namespace GameJam2020
{
    public interface IInteractable
    {
        void Interact();

        Highlighter Highlighter { get; }
    }
}
