﻿namespace GameJam2020
{
    using UnityEngine;

    public class Door : MonoBehaviour, IInteractable
    {
        [SerializeField]
        private Animator animator = null;

        [SerializeField]
        private Highlighter highlighter = null;

        private bool isOpen = false;
        public bool IsOpen => isOpen;

        public Highlighter Highlighter => highlighter;


        [ContextMenu("Open")]
        public void Open()
        {
            if (!isOpen)
            {
                isOpen = true;
                animator.SetTrigger("Open");
            }
        }

        [ContextMenu("Close")]
        public void Close()
        {
            if (isOpen)
            {
                isOpen = false;
                animator.SetTrigger("Close");
            }
        }

        public void Interact()
        {
            if (isOpen)
            {
                Close();
            }
            else
            {
                Open();
            }
        }
    }
}
