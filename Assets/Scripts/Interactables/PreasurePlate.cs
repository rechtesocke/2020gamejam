﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class PreasurePlate : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent onEnter = default;

        private void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody && !other.GetComponent<GhostController>())
            {
                onEnter?.Invoke();
            }
        }
    }
}
