﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public abstract class LivingEntity : MonoBehaviour, ITrackable
    {
        [SerializeField]
        protected UnityEvent onDeath = default;

        [SerializeField]
        protected UnityEvent onRewind = default;


        public bool IsAlive { get; protected set; } = true;


        public virtual void Die()
        {
            if (IsAlive)
            {
                IsAlive = false;
                onDeath?.Invoke();
            }
        }

        public abstract void Recreate(string name, object value);

        public virtual void Rewind()
        {
            onRewind?.Invoke();
        }

        public abstract void StartTracking();

        public abstract void StopTracker();
    }
}
