﻿namespace GameJam2020
{
    using UnityEngine;
    using UnityEngine.Events;

    public class Bunny : LivingEntity, System.IEquatable<Bunny>
    {
        [SerializeField]
        protected UnityEvent onRevive = default;

        [SerializeField]
        protected UnityEvent onFistDeath = default;        [SerializeField]
        protected UnityEvent onSecondDeath = default;

        private bool died = false;

        private void Awake()
        {
            gameObject.Register(this);
        }

        private void Start()
        {
            TimeValueTracker.Instance.RegisterTrackable(this);
        }

        public override void Die()
        {
            if (died)
            {
                onSecondDeath?.Invoke();
            }
            else
            {
                onFistDeath?.Invoke();
            }
            if (IsAlive)
            {
                IsAlive = false;
            }
            if (!died)
            {
                died = true;
                onDeath?.Invoke();
            }
        }

        public void Revive()
        {
            if (died)
            {
                IsAlive = true;
                died = false;
                onRevive?.Invoke();
            }
        }

        public override void StartTracking()
        {
            TimeValueTracker.Instance.TrackValue(new TimeValueNode(this, nameof(IsAlive), IsAlive));
        }

        public override void StopTracker() { }

        public override void Recreate(string name, object value)
        {
            switch (name)
            {
                case nameof(IsAlive): IsAlive = (bool)value; break;
            }
        }

        public bool Equals(Bunny other)
        {
            return GetInstanceID() == other.GetInstanceID();
        }
    }
}
