﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostAnim : MonoBehaviour
{
    private SkinnedMeshRenderer render;
    private int frameCounter = 0;
    private float[] keySource = new float[3];
    private float[] keyGoals = new float[3];
    private float[] changeSpeed = new float[3];

    public int updateRate;

    // Start is called before the first frame update
    void Start()
    {
        render = this.gameObject.GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (frameCounter <= updateRate) {
            frameCounter++;
        }
        else
        {
            frameCounter = 0;
            GenerateNewValue();
        }
        UpdateShapeKeys();
    }

    void UpdateShapeKeys()
    {
        float prevValue;
        for (int i = 0; i <= 2; i++)
        {
            prevValue = render.GetBlendShapeWeight(i);
            render.SetBlendShapeWeight(i, prevValue + changeSpeed[i]);            
        }
    }

    void GenerateNewValue() {
        for (int index = 0; index <= 2; index++) {
            float randVal = Random.Range(0f, 100f);
            keySource[index] = keyGoals[index];
            keyGoals[index] = randVal;
            changeSpeed[index] = (keyGoals[index] - keySource[index]) / updateRate;
        }
    }
}
